Dim oShell
Set oShell = WScript.CreateObject ("WScript.Shell")


Function GetFileLineN(file_name, line_number)
  Set objFileToRead = CreateObject("Scripting.FileSystemObject").OpenTextFile(file_name ,1)
  Dim iCounter
  iCounter = 1
  Dim strReturn
  do while not objFileToRead.AtEndOfStream
       strReturn = objFileToRead.ReadLine()
       If iCounter = line_number Then Exit Do
       iCounter = iCounter + 1
  loop
  objFileToRead.Close
  Set objFileToRead = Nothing

  GetFileLineN = strReturn
End Function

trans_dump = "C:\wamp\www\sprint\tmpTransIn.txt"



Dim zNumber
zNumber = GetFileLineN( trans_dump, 1 )

' If zNumber < 0 
' and Ignore transaction filterring (All tansactions considered good)
Dim blIgnoeFiltering
blIgnoeFiltering = false
if zNumber < 0  then
  blIgnoeFiltering = true
End If

oShell.run "cmd /c DecryptTranb.cmd " +  zNumber,9, true

' Create list of transmit UIDS
Const ForReading = 1 
Set objFSO = CreateObject("Scripting.FileSystemObject") 
Set objTextFile = objFSO.OpenTextFile _ 
    ("C:\wamp\www\sprint\zout\" + zNumber + ".z_payments.txt", ForReading) 

Set dictPayments = CreateObject("Scripting.Dictionary") 
Do Until objTextFile.AtEndOfStream 
    strNextLine = trim(objTextFile.Readline + 0) 


    if not dictPayments.Exists(strNextLine) then
      dictPayments.Add strNextLine, ""
    end if
    
Loop 



' Compare file
Set objTextFile = objFSO.OpenTextFile _ 
    ("C:\wamp\www\sprint\zout\" + zNumber + ".decryptedTranb.txt", ForReading) 


' Hwrite files
payments_good="C:\wamp\www\sprint\zout\" + zNumber + ".payments.good.txt"
Set obj_payments_good = objFSO.CreateTextFile(payments_good,True)
payments_bad="C:\wamp\www\sprint\zout\" + zNumber + ".payments.bad.txt"
Set obj_payments_bad = objFSO.CreateTextFile(payments_bad,True)


Do Until objTextFile.AtEndOfStream 
    strNextLine = trim(objTextFile.Readline) 
    dim sUid
    sUid = trim(Mid(strNextLine, 130, 30))
  

    if dictPayments.Exists(sUid) or blIgnoeFiltering then
    	obj_payments_good.Write strNextLine & vbCrLf
    else
    	obj_payments_bad.Write strNextLine & vbCrLf
    end if
Loop  

obj_payments_good.Close
obj_payments_bad.Close


oShell.run "cmd /c copy /Y " & payments_good & " " & "c:\sva\tran",9, true





