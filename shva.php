<?php
	
	define('__ROOT__', dirname(dirname(__FILE__))); 
	require_once(__ROOT__.'/sprint/config.php'); 
	require_once(__ROOT__.'/sprint/Pagebase.php'); 
	require_once(__ROOT__.'/sprint/File_reader.php'); 
	isAuthorize('shva');
	error_reporting(E_ALL); 
	
	class shva extends Pagebase {
		// interface credit card
		private static $INTR = "INTR";
		private static $INTR_file_in = 'tmpShvaIn.txt';
		private static $INTR_file_out = 'tmpShvaOut.txt';

		// daily transmission
		// Expect znumber or unique identifier in 'data'
		private static $TRANS = "TRANS";
		private static $TRANS_file_in = 'tmpTransIn.txt';
		// return daily transmission result
		private static $TRANS_OUT = "TRANS_OUT";

		// Is trans exists 
		private static $IS_TRAN_EXISTS = "IS_TRAN_EXISTS";
		private static $IS_TRAN_EXISTS_file_in = 'tmpIS_TRAN_EXISTS.txt';
		private static $IS_TRAN_EXISTS_file_out = 'tmpIS_TRAN_EXISTS_OUT.txt';		
		//force_trans
		private static $FORCE_TRANS = "FORCE_TRANS";
		private static $FORCE_TRANS_file_in = 'tmpFORCE_TRANS.txt';

	

		public function __construct($post) {  
			parent::__construct($post);
			
			$this->run_based_on_action();
		}



		private function print_file_diff(){
			$data = $this->get_ary_param_data( 'data' );

			$return_val = array();

			$hight_watter_mark = 'zout/'.$data.'.HighWatterMark.txt';
			$new_file = 'zout/'.$data.'.out.txt';
			

			$hwm = new File_reader($hight_watter_mark);
			$nf = new File_reader($new_file);
			

			try {
			    $return_val =  $nf->strlist_from_line( $hwm->line_count() + 1 ) ;
			} catch (Exception $e) {
			    array_push($return_val,'ERROR');
			    array_push($return_val,$e->getMessage());
			}

			echo json_encode( $return_val) ;
			
		}

		private function run_based_on_action(){
				//Decalare
				$action = $this->get_ary_param_data( 'action' );
				$data = $this->get_ary_param_data( 'data' );

				if ( $action == self::$INTR ) {
					$this->save_in_wait_for_out(
										self::$INTR_file_in, 
										self::$INTR_file_out, 
										'data'
										);
				} else if ( $action == self::$TRANS ) {
					// Here we request TRANS95.EXE
					// output file is transmitted in the 'data' string
					// cliem tries get file requested for every x seconds
					$this->save_post_param_to_file( 'data',  self::$TRANS_file_in );

					$z_payments_file = 'zout/'.$data.'.z_payments.txt';
					$this->save_post_param_to_file( 'payments', $z_payments_file );
					
				} else if ( $action == self::$TRANS_OUT ) {
					$this->print_file_diff();
				} else if( $action == self::$IS_TRAN_EXISTS ) {
					$this->save_in_wait_for_out(
										self::$IS_TRAN_EXISTS_file_in, 
										self::$IS_TRAN_EXISTS_file_out, 
										'data'
										);
				} else if( $action == self::$FORCE_TRANS ) {
					$this->save_post_param_to_file( 'data',  self::$FORCE_TRANS_file_in );
				}
				else {
					throw new Exception('Unknown action'.$action);
				}
		}



	}


	$shva = new shva( $_POST );

?>
