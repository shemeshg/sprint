<?php

  define('__ROOT__', dirname(dirname(__FILE__))); 
  require_once(__ROOT__.'/sprint/config.php'); 

$url = $_POST["URL"];
$data = $_POST["DATA"];
$method = $_POST["METHOD"];
$header = $_POST["HEADER"];

// use key 'http' even if you send the request to https://...
$options = array(
    'http' => array(
        'header'  => $header,
        'method'  => $method,
        'content' => $data,
    ),
);

$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);

echo $result;

?>