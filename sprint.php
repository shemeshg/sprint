<?php
	
	define('__ROOT__', dirname(dirname(__FILE__))); 
	require_once(__ROOT__.'/sprint/config.php'); 
	require_once(__ROOT__.'/sprint/Pagebase.php'); 
	isAuthorize('remote_printing');
	error_reporting(E_ALL); 

	class sprinter extends Pagebase {
		private static $file = 'tmpPrn.txt';
		private static $filecodepage = 'codepage.txt';

		public function __construct($post) {  
			parent::__construct($post);
			
			$this->save_post_param_to_file( 'data',  self::$file );
			$this->save_post_param_to_file( 'codepage',  self::$filecodepage );
		}
	}

	$sprinter = new sprinter( $_POST );
	print "OK";

?>
