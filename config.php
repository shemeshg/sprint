<?php
$services = array( 
	'remote_printing' => true,
	'shva' => true
);


function isAuthorize($service_name){
		global $services;
		if (!$services[$service_name]) {
		header("HTTP/1.1 500 Internal Server Error");
		throw new Exception($service_name.' Disabled in config.php');	
	}
}

function do_cors(){
	// respond to preflights
	if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {
	  // return only the headers and not the content
	  // only allow CORS if we're doing a GET - i.e. no saving for now.
	  if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']) && 
		($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'GET' ||
		$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'] == 'POST' 
		)) {
	    header('Access-Control-Allow-Origin: *');
	    header('Access-Control-Allow-Headers: Authorization, Origin, X-Requested-With, Content-Type, Accept');
	  }
	  exit;
	}

	// Allow CORS
	header("Access-Control-Allow-Origin: *");
	header('Access-Control-Allow-Credentials: true');    
	header("Access-Control-Allow-Methods: GET, POST, OPTIONS"); 
}
do_cors();
ini_get("allow_url_fopen");

?>