<?php
class File_reader {

		private $file_name = "";

		private function raise_err(){
			throw new Exception('Error on file '.$this->file_name);
		}

		public function __construct($file_name) { 
			$this->file_name = $file_name;
		}

		public function line_count(){
			$line_count = 0;
			$handle = @fopen($this->file_name, "r");
			if ($handle) {
			    while (($line = fgets($handle)) !== false) {
			        // process the line read.
			    	$line_count++;
			    }
			    fclose($handle);
			} else {
			    // error opening the file.
			    $this->raise_err();
			} 
			
			return $line_count;
		}

		public function strlist_from_line($ifrom_line){
			$return_val = array();

			$line_count = 0;
			$handle = @fopen($this->file_name, "r");

			if ($handle) {
			    while (($line = fgets($handle)) !== false) {
			        // process the line read.
			    	$line_count++;
			    	if ($ifrom_line <= $line_count) {
			    		array_push($return_val, $line);
			    	}
			    }
			    fclose($handle);
			} else {
			    // error opening the file.
			    $this->raise_err();
			} 
			return $return_val;
		}

}

?>