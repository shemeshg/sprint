Dim fso
Set fso = CreateObject("Scripting.FileSystemObject")
Dim prn_dump 
Dim shva_dump
dim trans_dump
dim codepage_dump
dim FORCE_TRANS_dump 
prn_dump = "C:\wamp\www\sprint\tmpPrn.txt"
shva_dump = "C:\wamp\www\sprint\tmpShvaIn.txt"
trans_dump = "C:\wamp\www\sprint\tmpTransIn.txt"
codepage_dump = "C:\wamp\www\sprint\codepage.txt"
FORCE_TRANS_dump = "C:\wamp\www\sprint\tmpFORCE_TRANS.txt"
IS_TRAN_EXISTS_dump = "tmpIS_TRAN_EXISTS.txt"
IS_TRAN_EXISTS_dump_out = "tmpIS_TRAN_EXISTS_OUT.txt"


Dim oShell
Set oShell = WScript.CreateObject ("WScript.Shell")

Function GetFileLineN(file_name, line_number)
  Set objFileToRead = CreateObject("Scripting.FileSystemObject").OpenTextFile(file_name ,1)
  Dim iCounter
  iCounter = 1
  Dim strReturn
  do while not objFileToRead.AtEndOfStream
       strReturn = objFileToRead.ReadLine()
       If iCounter = line_number Then Exit Do
       iCounter = iCounter + 1
  loop
  objFileToRead.Close
  Set objFileToRead = Nothing

  GetFileLineN = strReturn
End Function


Sub Engine_main
  
  ' Printer Engine
  If (fso.FileExists( prn_dump )) Then
    'Wscript.Echo "c:\wamp\www\sprint\SPRINT.EXE " +  prn_dump + " " + strCodePage
    dim strCodePage 
    strCodePage = GetFileLineN(codepage_dump ,1)
    oShell.run "c:\wamp\www\sprint\SPRINT.EXE " +  prn_dump + " " + strCodePage ,0, true
    fso.DeleteFile prn_dump 
  end if

  IF ( fso.FileExists(IS_TRAN_EXISTS_dump) ) Then
    
    IF (fso.FileExists( IS_TRAN_EXISTS_dump_out ) ) THEN
      fso.DeleteFile IS_TRAN_EXISTS_dump_out
    End If

    If (fso.FileExists( "c:\sva\tran" ) ) Then
      oShell.run "cmd /c echo true > " + IS_TRAN_EXISTS_dump_out,0, true
    Else
      oShell.run "cmd /c echo false > " + IS_TRAN_EXISTS_dump_out,0, true
    End If

    fso.DeleteFile IS_TRAN_EXISTS_dump
  End if

  ' Shva engine
  If (fso.FileExists( shva_dump )) Then
    'Wscript.Echo "C:\wamp\www\sprint\DoShva.cmd"
    oShell.run "C:\wamp\www\sprint\DoShva.cmd",0, true
    ' No need to delete - file already moved to c:\sva folder
    'fso.DeleteFile shva_dump 
  end if

  IF (fso.FileExists( trans_dump ) AND NOT fso.FileExists( "c:\sva\tran" ) ) THEN
    fso.DeleteFile trans_dump
  End If

  if ( fso.FileExists(FORCE_TRANS_dump) ) then
    oShell.run "C:\wamp\www\sprint\DoTrans.cmd ",9, true
    fso.DeleteFile FORCE_TRANS_dump 
  end if

  If (fso.FileExists( trans_dump ) ) Then

    '' clean tran file
    oShell.run "C:\wamp\www\sprint\PrepairTran.vbs",0, true

    '' Get transmision response
    Dim sOutFile
    sOutFile = GetFileLineN( trans_dump, 1 )
    Dim hwmFile
    hwmFile = "zout\" + sOutFile + ".HighWatterMark.txt"
    dim outputFile
    outputFile = "zout\" + sOutFile + ".out.txt"


    if ( Not fso.FileExists("c:\\sva\\STATIS") ) then
      fso.CreateTextFile("c:\\sva\\STATIS")
    end if
    If (Not fso.FileExists( hwmFile )) Then 
       fso.CopyFile "c:\\sva\\STATIS", hwmFile
    end if
    oShell.run "C:\wamp\www\sprint\DoTrans.cmd ",9, true
      fso.CopyFile "c:\\sva\\STATIS", outputFile
    fso.DeleteFile trans_dump 
  end if
End Sub


While True
  On Error Resume Next
  Engine_main

  if  Err.Number <> 0  then 
	msgbox "ERROR NUMBER:" & Err.Number & vbCrLf  & vbCrLf & "   DESCRIPTION:"& Err.Description
	Err.Clear
  end if
  WScript.Sleep 500
WEnd