<?php
class Pagebase {

		protected $post = "";

		public function __construct($post) { 
			$this->post = $post;
		}

		protected static function  file_delete_if_exists($file_name){
			if (file_exists($file_name)) {
		        unlink($file_name);
		    }			
		}

		protected static function wait_for_file_and_print($file_path){
			// Wait for file and print
			while (! file_exists($file_path)){
				sleep(1); 
			}
			$response_out = file_get_contents($file_path); 	
			print $response_out;
		}


		protected function save_in_wait_for_out($file_in, $file_out, $post_param){
			self::file_delete_if_exists( $file_out );
			$this->save_post_param_to_file( $post_param,  $file_in );
			
			self::wait_for_file_and_print( $file_out );
		} 


		protected function save_post_param_to_file($post_param, $file_name){
			$current = $this->get_ary_param_data( $post_param );
			file_put_contents($file_name, $current);
		}


		protected function get_ary_param_data($param){
			$current = "";
			if(isset($this->post[ $param ])) {
				$current = $this->post[ $param ];
			} else {
				throw new Exception('$_POST['.$param.'] does not exists');
			}
			return $current;
		}







}

?>